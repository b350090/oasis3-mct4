OASIS3-MCT license and copyright statement
------------------------------------------

Copyright © 2018 Centre Européen de Recherche et Formation
Avancée en Calcul Scientifique (CERFACS).  

This software and ancillary information called OASIS3-MCT is free
software.  CERFACS has rights to use, reproduce, and distribute
OASIS3-MCT. The public may copy, distribute, use, prepare derivative works
and publicly display OASIS3-MCT under the terms of the Lesser GNU General
Public License (LGPL) as published by the Free Software Foundation,
provided that this notice and any statement of authorship are
reproduced on all copies. If OASIS3-MCT is modified to produce derivative
works, such modified software should be clearly marked, so as not to
confuse it with the OASIS3-MCT version available from CERFACS.

The developers of the OASIS3-MCT software are researchers attempting to
build a modular and user-friendly coupler accessible to the climate
modelling community. Although we use the tool ourselves and have made
every effort to ensure its accuracy, we can not make any
guarantees. We provide the software to you for free. In return,
you - the user - assume full responsibility for use of the software. The
OASIS3-MCT software comes without any warranties (implied or expressed)
and is not guaranteed to work for you or on your computer. Specifically,
CERFACS and the various individuals involved in development and
maintenance of the OASIS3-MCT software are not responsible for any damage
that may result from correct or incorrect use of this software.

============================================================================

MCT copyright statement
-----------------------

              Modeling Coupling Toolkit (MCT) Software

Copyright © 2011, UChicago Argonne, LLC as Operator of Argonne
National Laboratory. All rights reserved. 

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution. 
3. The end-user documentation included with the redistribution, if
   any, must include the following acknowledgment: "This product includes
   software developed by the UChicago Argonne, LLC, as Operator of
   Argonne National Laboratory." Alternately, this acknowledgment may
   appear in the software itself, if and wherever such third-party
   acknowledgments normally appear. 
   This software was authored by:
   * Argonne National Laboratory Climate Modeling Group,
     Mathematics and Computer Science Division,
     Argonne National Laboratory
     Argonne IL 60439 
   * Robert Jacob, tel: (630) 252-2983
     E-mail: jacob@mcs.anl.gov
   * Jay Larson
     E-mail: larson@mcs.anl.gov
   * Everest Ong
   * Ray Loy
4. WARRANTY DISCLAIMER. THE SOFTWARE IS SUPPLIED "AS IS" WITHOUT
   WARRANTY OF ANY KIND. THE COPYRIGHT HOLDER, THE UNITED STATES, THE
   UNITED STATES DEPARTMENT OF ENERGY, AND THEIR EMPLOYEES: (1) DISCLAIM
   ANY WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY
   IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
   PURPOSE, TITLE OR NON-IN- FRINGEMENT, (2) DO NOT ASSUME ANY LEGAL
   LIABILITY OR RESPONSIBILITY FOR THE ACCURACY, COMPLETENESS, OR
   USEFULNESS OF THE SOFTWARE, (3) DO NOT REPRESENT THAT USE OF THE
   SOFTWARE WOULD NOT INFRINGE PRIVATELY OWNED RIGHTS, (4) DO NOT WARRANT
   THAT THE SOFTWARE WILL FUNCTION UNINTERRUPTED, THAT IT IS ERROR-FREE
   OR THAT ANY ERRORS WILL BE CORRECTED.  
5. LIMITATION OF LIABILITY. IN NO EVENT WILL THE COPYRIGHT HOLDER, THE
   UNITED STATES, THE UNITED STATES DEPARTMENT OF ENERGY, OR THEIR
   EMPLOYEES: BE LIABLE FOR ANY INDIRECT, INCIDENTAL, CONSEQUENTIAL,
   SPECIAL OR PUNITIVE DAMAGES OF ANY KIND OR NATURE, INCLUDING BUT NOT
   LIMITED TO LOSS OF PROFITS OR LOSS OF DATA, FOR ANY REASON WHATSOEVER,
   WHETHER SUCH LIABILITY IS ASSERTED ON THE BASIS OF CONTRACT, TORT
   (INCLUDING NEGLIGENCE OR STRICT LIABILITY), OR OTHERWISE, EVEN IF ANY
   OF SAID PARTIES HAS BEEN WARNED OF THE POSSIBILITY OF SUCH LOSS OR
   DAMAGES. 

============================================================================

The SCRIP 1.4 license copyright statement
-----------------------------------------

The SCRIP 1.4 copyright statement reads as follows:
"Copyright © 1997, 1998 the Regents of the University of California.
This software and ancillary information (herein called SOFTWARE)
called SCRIP is made available under the terms described here. The
SOFTWARE has been approved for release with associated LA-CC Number
98-45. Unless otherwise indicated, this SOFTWARE has been authored by
an employee or employees of the University of California, operator of
Los Alamos National Laboratory under Contract No. W-7405-ENG-36 with
the United States Department of Energy. The United States Government
has rights to use, reproduce, and distribute this SOFTWARE. The public
may copy, distribute, prepare derivative works and publicly display
this SOFTWARE without charge, provided that this Notice and any
statement of authorship are reproduced on all copies. Neither the
Government nor the University makes any warranty, express or implied,
or assumes any liability or responsibility for the use of this
SOFTWARE. If SOFTWARE is modified to produce derivative works, such
modified SOFTWARE should be clearly marked, so as not to confuse it
with the version available from Los Alamos National Laboratory."
