cmake_minimum_required(VERSION 3.4)

project(mct Fortran)

# get our source files
set(src_home ${CMAKE_CURRENT_LIST_DIR}) # path to src directory starting from the dir containing our CMakeLists.txt
file(GLOB all_sources ${src_home}/*.F90)

# depends on the mpeu library
add_subdirectory(../mpeu ${PROJECT_BINARY_DIR}/mpeu)

# create our library (set its name to name of this project)
add_library(${PROJECT_NAME} ${all_sources})
target_link_libraries(${PROJECT_NAME} mpeu)
target_include_directories(${PROJECT_NAME} INTERFACE ${PROJECT_BINARY_DIR}) # depending projects should include our *.mod files
target_compile_definitions(${PROJECT_NAME} PRIVATE use_libMPI use_netCDF use_comm_MPI1)
target_compile_options(${PROJECT_NAME} PRIVATE ${OASIS_FFLAGS})
