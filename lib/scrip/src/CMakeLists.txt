cmake_minimum_required(VERSION 3.4)

project(scrip Fortran)

# get our source files
set(src_home ${CMAKE_CURRENT_LIST_DIR}) # path to src directory starting from the dir containing our CMakeLists.txt
file(GLOB all_sources ${src_home}/*.F** ${src_home}/*.f**)
list(REMOVE_DUPLICATES all_sources) # cmake glob is not case sensitive on some platforms, so be sure to remove the duplicates

include(${CMAKE_CURRENT_LIST_DIR}/../../../cmake/FindNETCDF.cmake)

# create our library (set its name to name of this project)
add_library(${PROJECT_NAME} ${all_sources})
target_include_directories(${PROJECT_NAME}
PRIVATE ${NETCDF_Fortran_INCLUDE_DIRECTORIES}
INTERFACE ${PROJECT_BINARY_DIR} # depending projects should include our *.mod files
)
target_compile_definitions(${PROJECT_NAME} PRIVATE use_libMPI use_netCDF use_comm_MPI1)
target_compile_options(${PROJECT_NAME} PRIVATE ${OASIS_FFLAGS})
